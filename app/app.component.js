"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
function translit(str) {
    var dic = { 'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ж': 'zh', 'з': 'z', 'и': 'i', 'й': 'y', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o', 'п': 'p', 'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'ы': 'y', 'э': 'e', 'А': 'A', 'Б': 'B', 'В': 'V', 'Г': 'G', 'Д': 'D', 'Е': 'E', 'Ж': 'ZH', 'З': 'Z', 'И': 'I', 'Й': 'Y', 'К': 'K', 'Л': 'L', 'М': 'M', 'Н': 'N', 'О': 'O', 'П': 'P', 'Р': 'R', 'С': 'S', 'Т': 'T', 'У': 'U', 'Ф': 'F', 'Ы': 'Y', 'Э': 'E', 'ё': 'yo', 'х': 'h', 'ц': 'ts', 'ч': 'ch', 'ш': 'sh', 'щ': 'shch', 'ъ': '\'', 'ь': '\'', 'ю': 'yu', 'я': 'ya', 'Ё': 'YO', 'Х': 'H', 'Ц': 'TS', 'Ч': 'CH', 'Ш': 'SH', 'Щ': 'SHCH', 'Ъ': '\'', 'Ь': '\'', 'Ю': 'YU', 'Я': 'YA' };
    var replacer = function (a) { return dic[a] || a; };
    return str.replace(/[А-яёЁ]/g, replacer);
}
var AppComponent = (function () {
    function AppComponent() {
        this.sample = '';
        this.translit = false;
        this.title = 'Translit Component';
    }
    AppComponent.prototype.setTranslit = function (flag) {
        this.translit = flag;
    };
    ;
    AppComponent.prototype.getSample = function () {
        if (this.translit) {
            return translit(this.sample);
        }
        else {
            return this.sample;
        }
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            template: "\n  \n\t<h1>Translit Component</h1>\n\t\n\t<h3>\n\t\t{{getSample()}}\n\t</h3>\n\t\n\t<div class=\"form-group\">\n\t\t<input [(ngModel)]=\"sample\" class=\"form-control\"  placeholder=\"Enter a sample string\">\n\t</div>\n  \n\t<div class=\"btn-group\" role=\"group\">\n\t  <button type=\"button\" class=\"btn btn-default active\"\n\t\t[ngClass]=\"{active: !translit}\"\n\t\t(click) = \"setTranslit(false)\"\n\t  >Original</button>\n\t  <button type=\"button\" class=\"btn btn-default\"\n\t\t[ngClass]=\"{active: translit}\"\n\t\t(click) = \"setTranslit(true)\"\n\t  >Transliterated</button>\n\t</div>\n  "
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map