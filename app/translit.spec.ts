import { AppComponent }  from './app.component';

describe('Translit Component', () => {

	var component;

    beforeEach( () => {	
		component = new AppComponent;
    });
	
	it('has title', () => {
        expect(component.title).toEqual('Translit Component');
    });
	
	it('has traslit flag set to false initially', () => {
        expect(component.translit).toEqual(false);
    });
	
	it('does not translit in Original mode', () => {
		component.sample = 'Проверка связи';
        expect(component.getSample()).toEqual(component.sample);
    });
	
	it('translits in Transliterate mode', () => {
		component.translit = true;
		component.sample = 'женщины и мужчины';
		expect(component.getSample()).toEqual('zhenshchiny i muzhchiny');
	});
	
	it('Leaves latin sample as is in Transliterate mode', () => {
		component.translit = true;
		component.sample = 'The quick brown fox jumps over the lazy dog';
		expect(component.getSample()).toEqual(component.sample);
	});
	
});