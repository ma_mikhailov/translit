import { Component } from '@angular/core';
import { NgClass } from '@angular/common';

function translit(str){
	
	var dic={'а':'a', 'б':'b', 'в':'v', 'г':'g', 'д':'d', 'е':'e', 'ж':'zh', 'з':'z', 'и':'i', 'й':'y', 'к':'k', 'л':'l', 'м':'m', 'н':'n', 'о':'o', 'п':'p', 'р':'r', 'с':'s', 'т':'t', 'у':'u', 'ф':'f', 'ы':'y', 'э':'e', 'А':'A', 'Б':'B', 'В':'V', 'Г':'G', 'Д':'D', 'Е':'E', 'Ж':'ZH', 'З':'Z', 'И':'I', 'Й':'Y', 'К':'K', 'Л':'L', 'М':'M', 'Н':'N', 'О':'O', 'П':'P', 'Р':'R', 'С':'S', 'Т':'T', 'У':'U', 'Ф':'F', 'Ы':'Y', 'Э':'E', 'ё':'yo', 'х':'h', 'ц':'ts', 'ч':'ch', 'ш':'sh', 'щ':'shch', 'ъ':'\'', 'ь':'\'', 'ю':'yu', 'я':'ya', 'Ё':'YO', 'Х':'H', 'Ц':'TS', 'Ч':'CH', 'Ш':'SH', 'Щ':'SHCH', 'Ъ':'\'', 'Ь':'\'','Ю':'YU', 'Я':'YA'};

	var replacer=function(a){return dic[a] || a};

	return str.replace(/[А-яёЁ]/g, replacer);

}

@Component({
  selector: 'my-app',
  
  template:`
  
	<h1>Translit Component</h1>
	
	<h3>
		{{getSample()}}
	</h3>
	
	<div class="form-group">
		<input [(ngModel)]="sample" class="form-control"  placeholder="Enter a sample string">
	</div>
  
	<div class="btn-group" role="group">
	  <button type="button" class="btn btn-default active"
		[ngClass]="{active: !translit}"
		(click) = "setTranslit(false)"
	  >Original</button>
	  <button type="button" class="btn btn-default"
		[ngClass]="{active: translit}"
		(click) = "setTranslit(true)"
	  >Transliterated</button>
	</div>
  `

})

export class AppComponent {
	sample: string = '';
	translit = false;
	title = 'Translit Component';
	
	setTranslit(flag: boolean): void {
	  this.translit = flag;
	};
	
	getSample(): string {
		if(this.translit)
		{
			return translit(this.sample);
		}
		else
		{
			return this.sample;
		}
	}
}
