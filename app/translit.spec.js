"use strict";
var app_component_1 = require('./app.component');
describe('Translit Component', function () {
    var component;
    beforeEach(function () {
        component = new app_component_1.AppComponent;
    });
    it('has title', function () {
        expect(component.title).toEqual('Translit Component');
    });
    it('has traslit flag set to false initially', function () {
        expect(component.translit).toEqual(false);
    });
    it('does not translit in Original mode', function () {
        component.sample = 'Проверка связи';
        expect(component.getSample()).toEqual(component.sample);
    });
    it('translits in Transliterate mode', function () {
        component.translit = true;
        component.sample = 'женщины и мужчины';
        expect(component.getSample()).toEqual('zhenshchiny i muzhchiny');
    });
    it('Leaves latin sample as is in Transliterate mode', function () {
        component.translit = true;
        component.sample = 'The quick brown fox jumps over the lazy dog';
        expect(component.getSample()).toEqual(component.sample);
    });
});
//# sourceMappingURL=translit.spec.js.map